#!/bin/bash

FILE_PATH=$1;
if [ -z "$1" ]
  then
    FILE_PATH="data/seed"
fi

bin/console doc:data:drop --if-exists --force
bin/console doc:data:create --if-not-exists
bin/console doc:mig:mig -n

bin/console ka:factory:init $FILE_PATH/factories.yaml