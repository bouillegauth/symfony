<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191122132833 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE factory (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(80) NOT NULL, code VARCHAR(8) NOT NULL, address LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_FB361EF95E237E06 (name), UNIQUE INDEX UNIQ_FB361EF977153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sec_refresh_token (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', login_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', impersonation_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, token VARCHAR(255) NOT NULL, expires_at DATETIME NOT NULL, api_token TINYINT(1) NOT NULL, valid TINYINT(1) NOT NULL, INDEX IDX_573E033DA76ED395 (user_id), INDEX IDX_573E033D5CB2E05D (login_id), INDEX IDX_573E033D26F87DB8 (impersonation_id), INDEX IDX_573E033D5F37A13BF9D83E28C0735FF (token, expires_at, valid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sec_login_result (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, login VARCHAR(120) NOT NULL, successful TINYINT(1) NOT NULL, device VARCHAR(60) NOT NULL, ip_address VARCHAR(20) NOT NULL, INDEX IDX_2A31DF86A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sec_impersonation (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', from_user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', to_user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_959EC9152130303A (from_user_id), INDEX IDX_959EC91529F6EE60 (to_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sec_user (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', created_by_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, first_name VARCHAR(60) NOT NULL, last_name VARCHAR(60) NOT NULL, email VARCHAR(120) NOT NULL, password VARCHAR(255) NOT NULL, salt VARCHAR(40) NOT NULL, avatar_url LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_68706E3E7927C74 (email), INDEX IDX_68706E3B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sec_access_token (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', parent_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', refresh_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', login_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', impersonation_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, token VARCHAR(255) NOT NULL, expires_at DATETIME NOT NULL, api_token TINYINT(1) NOT NULL, valid TINYINT(1) NOT NULL, INDEX IDX_789DE467727ACA70 (parent_id), INDEX IDX_789DE467EB68563E (refresh_id), INDEX IDX_789DE467A76ED395 (user_id), INDEX IDX_789DE4675CB2E05D (login_id), INDEX IDX_789DE46726F87DB8 (impersonation_id), INDEX IDX_789DE4675F37A13BF9D83E28C0735FF (token, expires_at, valid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sec_user_factory (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', factory_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_6D0439FCA76ED395 (user_id), INDEX IDX_6D0439FCC7AF27D2 (factory_id), UNIQUE INDEX UNIQ_6D0439FCA76ED395C7AF27D2 (user_id, factory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', company_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', factory_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, first_name VARCHAR(40) NOT NULL, last_name VARCHAR(40) NOT NULL, email VARCHAR(100) NOT NULL, middle_name VARCHAR(40) DEFAULT NULL, telephone VARCHAR(16) DEFAULT NULL, function VARCHAR(60) DEFAULT NULL, INDEX IDX_4C62E638979B1AD6 (company_id), INDEX IDX_4C62E638C7AF27D2 (factory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', parent_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', factory_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, name VARCHAR(80) NOT NULL, address LONGTEXT NOT NULL, INDEX IDX_4FBF094F727ACA70 (parent_id), INDEX IDX_4FBF094FC7AF27D2 (factory_id), UNIQUE INDEX UNIQ_4FBF094F5E237E06C7AF27D2 (name, factory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sec_refresh_token ADD CONSTRAINT FK_573E033DA76ED395 FOREIGN KEY (user_id) REFERENCES sec_user (id)');
        $this->addSql('ALTER TABLE sec_refresh_token ADD CONSTRAINT FK_573E033D5CB2E05D FOREIGN KEY (login_id) REFERENCES sec_login_result (id)');
        $this->addSql('ALTER TABLE sec_refresh_token ADD CONSTRAINT FK_573E033D26F87DB8 FOREIGN KEY (impersonation_id) REFERENCES sec_impersonation (id)');
        $this->addSql('ALTER TABLE sec_login_result ADD CONSTRAINT FK_2A31DF86A76ED395 FOREIGN KEY (user_id) REFERENCES sec_user (id)');
        $this->addSql('ALTER TABLE sec_impersonation ADD CONSTRAINT FK_959EC9152130303A FOREIGN KEY (from_user_id) REFERENCES sec_user (id)');
        $this->addSql('ALTER TABLE sec_impersonation ADD CONSTRAINT FK_959EC91529F6EE60 FOREIGN KEY (to_user_id) REFERENCES sec_user (id)');
        $this->addSql('ALTER TABLE sec_user ADD CONSTRAINT FK_68706E3B03A8386 FOREIGN KEY (created_by_id) REFERENCES sec_user (id)');
        $this->addSql('ALTER TABLE sec_access_token ADD CONSTRAINT FK_789DE467727ACA70 FOREIGN KEY (parent_id) REFERENCES sec_access_token (id)');
        $this->addSql('ALTER TABLE sec_access_token ADD CONSTRAINT FK_789DE467EB68563E FOREIGN KEY (refresh_id) REFERENCES sec_refresh_token (id)');
        $this->addSql('ALTER TABLE sec_access_token ADD CONSTRAINT FK_789DE467A76ED395 FOREIGN KEY (user_id) REFERENCES sec_user (id)');
        $this->addSql('ALTER TABLE sec_access_token ADD CONSTRAINT FK_789DE4675CB2E05D FOREIGN KEY (login_id) REFERENCES sec_login_result (id)');
        $this->addSql('ALTER TABLE sec_access_token ADD CONSTRAINT FK_789DE46726F87DB8 FOREIGN KEY (impersonation_id) REFERENCES sec_impersonation (id)');
        $this->addSql('ALTER TABLE sec_user_factory ADD CONSTRAINT FK_6D0439FCA76ED395 FOREIGN KEY (user_id) REFERENCES sec_user (id)');
        $this->addSql('ALTER TABLE sec_user_factory ADD CONSTRAINT FK_6D0439FCC7AF27D2 FOREIGN KEY (factory_id) REFERENCES factory (id)');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638C7AF27D2 FOREIGN KEY (factory_id) REFERENCES factory (id)');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F727ACA70 FOREIGN KEY (parent_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FC7AF27D2 FOREIGN KEY (factory_id) REFERENCES factory (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sec_user_factory DROP FOREIGN KEY FK_6D0439FCC7AF27D2');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E638C7AF27D2');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FC7AF27D2');
        $this->addSql('ALTER TABLE sec_access_token DROP FOREIGN KEY FK_789DE467EB68563E');
        $this->addSql('ALTER TABLE sec_refresh_token DROP FOREIGN KEY FK_573E033D5CB2E05D');
        $this->addSql('ALTER TABLE sec_access_token DROP FOREIGN KEY FK_789DE4675CB2E05D');
        $this->addSql('ALTER TABLE sec_refresh_token DROP FOREIGN KEY FK_573E033D26F87DB8');
        $this->addSql('ALTER TABLE sec_access_token DROP FOREIGN KEY FK_789DE46726F87DB8');
        $this->addSql('ALTER TABLE sec_refresh_token DROP FOREIGN KEY FK_573E033DA76ED395');
        $this->addSql('ALTER TABLE sec_login_result DROP FOREIGN KEY FK_2A31DF86A76ED395');
        $this->addSql('ALTER TABLE sec_impersonation DROP FOREIGN KEY FK_959EC9152130303A');
        $this->addSql('ALTER TABLE sec_impersonation DROP FOREIGN KEY FK_959EC91529F6EE60');
        $this->addSql('ALTER TABLE sec_user DROP FOREIGN KEY FK_68706E3B03A8386');
        $this->addSql('ALTER TABLE sec_access_token DROP FOREIGN KEY FK_789DE467A76ED395');
        $this->addSql('ALTER TABLE sec_user_factory DROP FOREIGN KEY FK_6D0439FCA76ED395');
        $this->addSql('ALTER TABLE sec_access_token DROP FOREIGN KEY FK_789DE467727ACA70');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E638979B1AD6');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094F727ACA70');
        $this->addSql('DROP TABLE factory');
        $this->addSql('DROP TABLE sec_refresh_token');
        $this->addSql('DROP TABLE sec_login_result');
        $this->addSql('DROP TABLE sec_impersonation');
        $this->addSql('DROP TABLE sec_user');
        $this->addSql('DROP TABLE sec_access_token');
        $this->addSql('DROP TABLE sec_user_factory');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE company');
    }
}
