<?php
declare(strict_types=1);

namespace KA\Contact\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use KA\Factory\Entity\Factory;
use KA\Factory\Entity\FactoryEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Defines the modelling for a company entity.
 *
 * @package KA\Contact\Entity
 * @author  Petre Pătrașc <petre@dreamlabs.ro>
 *
 * @ORM\Entity()
 * @ApiResource()
 * @UniqueEntity(fields={"name", "factory"})
 * @ORM\Table(name="company", uniqueConstraints={
 *      @ORM\UniqueConstraint(columns={"name", "factory_id"})
 * })
 */
class Company extends FactoryEntity
{
    /**
     * Name of the company.
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="2", max="80")
     */
    private $name;
    
    /**
     * Address of the company.
     *
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="6")
     */
    private $address;
    
    /**
     * Parent/holding company of the company instance.
     *
     * @var Company|null
     *
     * @ORM\ManyToOne(targetEntity="KA\Contact\Entity\Company", inversedBy="children")
     */
    private $parent;
    
    /**
     * Child companies of the company instance.
     *
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="KA\Contact\Entity\Company", mappedBy="parent")
     */
    private $children;
    
    /**
     * Contacts associated to the company
     *
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="KA\Contact\Entity\Contact", mappedBy="company")
     */
    private $contacts;
    
    /**
     * Company constructor.
     *
     * @param string       $name
     * @param string       $address
     * @param Company|null $parent
     */
    public function __construct(string $name, string $address, ?Company $parent = null)
    {
        $this->name    = $name;
        $this->address = $address;
        $this->parent  = $parent;
        
        $this->children = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }
    
    /**
     * Get the value of the name property.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * Set the value of the name property.
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName(string $name): Company
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * Get the value of the address property.
     *
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }
    
    /**
     * Set the value of the address property.
     *
     * @param string $address
     *
     * @return Company
     */
    public function setAddress(string $address): Company
    {
        $this->address = $address;
        
        return $this;
    }
    
    /**
     * Get the value of the parent property.
     *
     * @return Company|null
     */
    public function getParent(): ?Company
    {
        return $this->parent;
    }
    
    /**
     * Get the value of the children property.
     *
     * @return Company[]
     */
    public function getChildren(): array
    {
        return $this->children->toArray();
    }
    
    /**
     * Set the value of the parent property.
     *
     * @param Company|null $parent
     *
     * @return Company
     */
    public function setParent(?Company $parent): Company
    {
        $this->parent = $parent;
        
        return $this;
    }
    
    /**
     * Get the value of the contacts property.
     *
     * @return Contact[]
     */
    public function getContacts(): array
    {
        return $this->contacts->toArray();
    }
}