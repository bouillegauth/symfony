<?php
declare(strict_types=1);

namespace KA\Contact\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use KA\Factory\Entity\FactoryEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Defines the modelling for a contact that is associated to a company.
 *
 * @package KA\Contact\Entity
 * @author  Petre Pătrașc <petre@dreamlabs.ro>
 *
 * @ORM\Entity()
 * @ApiResource()
 */
class Contact extends FactoryEntity
{
    /**
     * Company that the contact is attached to
     *
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="KA\Contact\Entity\Company", inversedBy="contacts")
     * @Assert\NotNull()
     */
    private $company;
    
    /**
     * First name of the contact
     *
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=40)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="1", max="40")
     */
    private $firstName;
    
    /**
     * Last name of the contact
     *
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=40)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="1", max="40")
     */
    private $lastName;
    
    /**
     * Email of the contact
     *
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="6", max="100")
     * @Assert\Email()
     */
    private $email;
    
    /**
     * Middle name of the contact
     *
     * @var string|null
     *
     * @ORM\Column(name="middle_name", type="string", length=40, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(min="1", max="40")
     */
    private $middleName;
    
    /**
     * Telephone number of the contact
     *
     * @var string|null
     *
     * @ORM\Column(name="telephone", type="string", length=16, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(min="2", max="16")
     */
    private $telephone;
    
    /**
     * Function of the contact within the company
     *
     * @var string|null
     *
     * @ORM\Column(name="function", type="string", length=60, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(min="2", max="60")
     */
    private $function;
    
    /**
     * Contact constructor.
     *
     * @param Company     $company
     * @param string      $firstName
     * @param string      $lastName
     * @param string      $email
     * @param string|null $middleName
     * @param string|null $telephone
     * @param string|null $function
     */
    public function __construct(
        Company $company,
        string $firstName,
        string $lastName,
        string $email,
        ?string $middleName,
        ?string $telephone,
        ?string $function
    ) {
        $this->company    = $company;
        $this->firstName  = $firstName;
        $this->lastName   = $lastName;
        $this->email      = $email;
        $this->middleName = $middleName;
        $this->telephone  = $telephone;
        $this->function   = $function;
    }
    
    /**
     * Get the value of the company property.
     *
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }
    
    /**
     * Get the value of the firstName property.
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }
    
    /**
     * Get the value of the lastName property.
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }
    
    /**
     * Get the value of the email property.
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    
    /**
     * Get the value of the middleName property.
     *
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }
    
    /**
     * Get the value of the telephone property.
     *
     * @return string|null
     */
    public function getTelephone(): ?string
    {
        return $this->telephone;
    }
    
    /**
     * Get the value of the function property.
     *
     * @return string|null
     */
    public function getFunction(): ?string
    {
        return $this->function;
    }
    
    /**
     * Set the value of the company property.
     *
     * @param Company $company
     *
     * @return Contact
     */
    public function setCompany(Company $company): Contact
    {
        $this->company = $company;
        
        return $this;
    }
    
    /**
     * Set the value of the firstName property.
     *
     * @param string $firstName
     *
     * @return Contact
     */
    public function setFirstName(string $firstName): Contact
    {
        $this->firstName = $firstName;
        
        return $this;
    }
    
    /**
     * Set the value of the lastName property.
     *
     * @param string $lastName
     *
     * @return Contact
     */
    public function setLastName(string $lastName): Contact
    {
        $this->lastName = $lastName;
        
        return $this;
    }
    
    /**
     * Set the value of the email property.
     *
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail(string $email): Contact
    {
        $this->email = $email;
        
        return $this;
    }
    
    /**
     * Set the value of the middleName property.
     *
     * @param string|null $middleName
     *
     * @return Contact
     */
    public function setMiddleName(?string $middleName): Contact
    {
        $this->middleName = $middleName;
        
        return $this;
    }
    
    /**
     * Set the value of the telephone property.
     *
     * @param string|null $telephone
     *
     * @return Contact
     */
    public function setTelephone(?string $telephone): Contact
    {
        $this->telephone = $telephone;
        
        return $this;
    }
    
    /**
     * Set the value of the function property.
     *
     * @param string|null $function
     *
     * @return Contact
     */
    public function setFunction(?string $function): Contact
    {
        $this->function = $function;
        
        return $this;
    }
}