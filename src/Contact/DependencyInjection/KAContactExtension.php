<?php
declare(strict_types=1);

namespace KA\Contact\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Bundle service definition and semantic configuration processing.
 *
 * @package KA\Contact\DependencyInjection
 * @author  Petre Pătrașc <petre@dreamlabs.ro>
 */
class KAContactExtension extends Extension
{
    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $this->processConfiguration(new KAContactConfiguration(), $configs);
        $this->processServiceDefinitions($container);
    }
    
    /**
     * Handles the processing of service definitions.
     *
     * @param ContainerBuilder $container
     *
     * @throws \Exception
     */
    private function processServiceDefinitions(ContainerBuilder $container): void
    {
        $definitionFiles = [
//            'dal.yaml',
//            'bll.yaml',
        ];
        
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        
        foreach ($definitionFiles as $file) {
            $loader->load($file);
        }
    }
}