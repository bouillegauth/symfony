<?php
declare(strict_types=1);

namespace KA\Contact\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Bundle semantic configuration.
 *
 * @package KA\Contact\DependencyInjection
 * @author  Petre Pătrașc <petre@dreamlabs.ro>
 */
class KAContactConfiguration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('ka_contact');
        
        return $treeBuilder;
    }
}
