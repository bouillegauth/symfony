<?php
declare(strict_types=1);

namespace KA\Contact;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle definition, inheritance and configuration.
 *
 * @package KA\Contact
 * @author  Petre Pătrașc <petre@dreamlabs.ro>
 */
class KAContactBundle extends Bundle
{
    
}